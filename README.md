# A C++ library for a generic handing of multithreading of classes and functions

## Contributors

* Emil Fresk

---

## License

Licensed under the LGPL-v3 license, see LICENSE file for details.

---

