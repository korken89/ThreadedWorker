/****************************************************************************
*
* Copyright (C) 2015 Emil Fresk.
* All rights reserved.
*
* This file is part of the SerialPipe library.
*
* GNU Lesser General Public License Usage
* This file may be used under the terms of the GNU Lesser
* General Public License version 3.0 as published by the Free Software
* Foundation and appearing in the file LICENSE included in the
* packaging of this file.  Please review the following information to
* ensure the GNU Lesser General Public License version 3.0 requirements
* will be met: http://www.gnu.org/licenses/lgpl-3.0.html.
*
* If you have questions regarding the use of this file, please contact
* Emil Fresk at emil.fresk@gmail.com.
*
****************************************************************************/

#include <iostream>
#include <string>

#include "ClassWorkerPool.h"

using namespace std;

class MyWork : public WorkBase
{
private:
  int i = 0;

  mutex cout_access;

public:
  void RunWork()
  {
    lock_guard<mutex> locker(cout_access);
    cout << "I'm working!!!! " << i << endl;
    i++;
  }
};

std::queue<std::vector<int>> q1;

void add_to_queue(std::vector<int>& v) { q1.emplace(std::move(v)); }
int main()
{
  /* Create the worker, will be a thread collection in the future */
  ClassWorkerPool work(4);

  cout << "Num workers: " << work.numWorkers() << endl;

  /* Create a shared pointer object of the class */
  shared_ptr<MyWork> p = make_shared<MyWork>();

  /* Enqueue the work */
  work.enqueue_work(p);
  work.enqueue_work(p);
  work.enqueue_work(p);
  work.enqueue_work(p);
  work.enqueue_work(p);
  work.enqueue_work(p);
  work.enqueue_work(p);
  work.enqueue_work(p);
  work.enqueue_work(p);
  work.enqueue_work(p);
  work.enqueue_work(p);
  work.enqueue_work(p);
  work.enqueue_work(p);
  work.enqueue_work(p);

  return 0;
}
