/****************************************************************************
*
* Copyright (C) 2015 Emil Fresk.
* All rights reserved.
*
* This file is part of the SerialPipe library.
*
* GNU Lesser General Public License Usage
* This file may be used under the terms of the GNU Lesser
* General Public License version 3.0 as published by the Free Software
* Foundation and appearing in the file LICENSE included in the
* packaging of this file.  Please review the following information to
* ensure the GNU Lesser General Public License version 3.0 requirements
* will be met: http://www.gnu.org/licenses/lgpl-3.0.html.
*
* If you have questions regarding the use of this file, please contact
* Emil Fresk at emil.fresk@gmail.com.
*
****************************************************************************/

#ifndef _CLASS_WORKERPOOL_H
#define _CLASS_WORKERPOOL_H

#include <queue>
#include <vector>

#include <condition_variable>
#include <future>
#include <mutex>
#include <thread>

/**
 * @brief  Abstract class to force the implementation of RunWork().
 */
class WorkBase
{
public:
  virtual void RunWork() = 0;
};

/**
 * @brief A class to implement concurrent execution of threads. It is based on
 *        a wrapper class to provide a standardized way to create workers.
 */
class ClassWorkerPool
{
private:
  /** @brief List of worker threads for bookkeeping */
  std::vector<std::thread> workers;

  /** @brief Condition variable for synchronizing worker starts */
  std::condition_variable synchronizer;

  /** @brief Queue of functions/tasks to be executed by the workers */
  std::queue<std::shared_ptr<WorkBase>> work_queue;

  /** @brief Mutex for task queue access */
  std::mutex queue_access;

  /** @brief ClassWorkerPool shutdown indicator */
  std::atomic<bool> shutdown;

public:
  /**
   * @brief					Constructor for the ClassWorkerPool class. Will spawn
   *							the number threads specified in the constructor or
   *                          default to the number of cores available if 0.
   *
   * @param[in] num_workers   The number of concurrent workers to spawn.
   */
  ClassWorkerPool(std::size_t num_workers = 0) : shutdown(false)
  {
    /* Check that number of workers is more than one, else number of cores. */
    if (num_workers == 0) num_workers = std::thread::hardware_concurrency();

    /* Spawn the number of workers requested */
    for (auto i = 0; i < num_workers; i++)
    {
      /* Add each worker to the vector of workers. */
      workers.emplace_back([this, i]() {

        std::shared_ptr<WorkBase> work;

        while (1)
        {
          {
            /* Lock and wait for resume conditions */
            std::unique_lock<std::mutex> locker(queue_access);
            synchronizer.wait(locker, [this]() {
              return ((shutdown == true) || !work_queue.empty());
            });

            /* Shutdown requested, terminate if the queue is empty */
            if ((shutdown == true) && work_queue.empty()) return;

            /* Get the oldest task from the queue to be executed
            and remove it from the queue */
            work = std::move(work_queue.front());
            work_queue.pop();
          }

          work->RunWork();
        }
      });
    }
  }

  /**
   * @brief		Destructor for the ClassWorkerPool class. Will send the shutdown
   *				signal to all threads and wait for them to terminate close.
   */
  ~ClassWorkerPool()
  {
    /* Send the shutdown signal */
    {
      std::lock_guard<std::mutex> locker(queue_access);
      shutdown = true;
    }

    /* Wake all workers */
    synchronizer.notify_all();

    /* Wait for all threads to terminate */
    for (auto &worker : workers) worker.join();
  }

  /**
   * @brief            Enqueues a class for execution by the workers.
   *
   * @param[in] work   Shared pointer to class containing the Run()
   * implementation.
   */
  inline void enqueue_work(std::shared_ptr<WorkBase> work)
  {
    /* Enqueue the class */
    {
      std::lock_guard<std::mutex> locker(queue_access);

      if (shutdown != true) work_queue.emplace(std::ref(work));
    }

    /* Wake a worker if there is one available */
    synchronizer.notify_one();
  }

  /**
   * @brief     Returns the number of workers.
   *
   * @return 		The number of workers.
   */
  std::size_t numWorkers() { return workers.size(); }
};

#endif
