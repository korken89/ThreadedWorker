/****************************************************************************
*
* Copyright (C) 2015 Emil Fresk.
* All rights reserved.
*
* This file is part of the SerialPipe library.
*
* GNU Lesser General Public License Usage
* This file may be used under the terms of the GNU Lesser
* General Public License version 3.0 as published by the Free Software
* Foundation and appearing in the file LICENSE included in the
* packaging of this file.  Please review the following information to
* ensure the GNU Lesser General Public License version 3.0 requirements
* will be met: http://www.gnu.org/licenses/lgpl-3.0.html.
*
* If you have questions regarding the use of this file, please contact
* Emil Fresk at emil.fresk@gmail.com.
*
****************************************************************************/

#ifndef _FUNC_WORKERPOOL_H
#define _FUNC_WORKERPOOL_H

#include <queue>
#include <vector>

#include <condition_variable>
#include <future>
#include <mutex>
#include <thread>

/**
 * @brief A class to implement concurrent execution of threads. It takes
 *        practically any function and return a std::future to retrieve the
 *        result.
 */
class FuncWorkerPool
{
private:
  /** @brief List of worker threads for bookkeeping */
  std::vector<std::thread> workers;

  /** @brief Condition variable for synchronizing worker starts */
  std::condition_variable synchronizer;

  /** @brief Queue of functions/tasks to be executed by the workers */
  std::queue<std::function<void()>> work_queue;

  /** @brief Mutex for task queue access */
  std::mutex queue_access;

  /** @brief WorkerPool shutdown indicator */
  std::atomic<bool> shutdown;

public:
  /**
   * @brief                   Constructor for the FuncWorkerPool class. Will
   *                          spawn the number threads specified in the
   *                          constructor or default to the number of cores
   *                          available.
   *
   * @param[in] num_workers   The number of concurrent workers to spawn.
   */
  FuncWorkerPool(std::size_t num_workers = 0)
  {
    /* Check that number of workers is more than one, else number of cores. */
    if (num_workers == 0) num_workers = std::thread::hardware_concurrency();

    /* Spawn the number of workers requested */
    for (auto i = 0; i < num_workers; i++)
    {
      /* Add each worker to the vector of workers. */
      workers.emplace_back([this, i]() {

        /* Worker's task implementation (void function) */
        std::function<void()> running_task;

        while (1)
        {
          {
            /* Lock and wait for resume conditions */
            std::unique_lock<std::mutex> locker(queue_access);
            synchronizer.wait(locker, [this]() {
              return ((shutdown == true) || !work_queue.empty());
            });

            /* Shutdown requested, terminate if the queue is empty */
            if ((shutdown == true) && work_queue.empty()) return;

            /* Get the oldest task from the queue to be executed
               and remove it from the queue */
            running_task = std::move(work_queue.front());
            work_queue.pop();
          }

          running_task();
        }
      });
    }
  }

  /**
   * @brief   Destructor for the FuncWorkerPool class. Will send the shutdown
   *          signal to all threads and wait for them to terminate close.
   */
  ~FuncWorkerPool()
  {
    /* Send the shutdown signal */
    {
      std::lock_guard<std::mutex> locker(queue_access);
      shutdown = true;
    }

    /* Wake all workers */
    synchronizer.notify_all();

    /* Wait for all threads to terminate */
    for (auto& worker : workers) worker.join();
  }

  /**
   * @brief            Enqueues a function for execution by the workers.
   *                   Use get() to get the return value of the function.
   *
   * @param[in] fun    Function to give to the workers.
   * @param[in] args   Arguments to the function.
   * @return           Returns a std::future linked to the function, use
   *                   get() to get the return value of the function.
   */
  template <class F, class... Arguments>
  inline auto enqueue_work(F&& fun, Arguments&&... args)
      -> std::future<typename std::result_of<F(Arguments...)>::type>
  {
    /* Extract the return type of the function */
    using result_of_F = typename std::result_of<F(Arguments...)>::type;

    /* Package the task for the queue.
     * 1. Use shared pointer for the function object to exist until execution.
     * 2. Use a packed_task to package the function for easy moving.
     * 3. Use bind to remove the arguments.
     * 4. Register the future value so it can be viewed as a void() function.
     */
    auto task = std::make_shared<std::packaged_task<result_of_F()>>(
        std::bind(fun, std::forward<Arguments>(args)...));

    /* Capture the future return value of the function */
    std::future<result_of_F> result = task->get_future();

    /* Enqueue the task */
    {
      std::lock_guard<std::mutex> locker(queue_access);

      if (shutdown != true) work_queue.emplace([task]() { (*task)(); });
    }

    /* Wake a worker if there is one available */
    synchronizer.notify_one();

    return result;
  }

  /**
   * @brief     Returns the number of workers.
   *
   * @return 		The number of workers.
   */
  std::size_t numWorkers() { return workers.size(); }
};

#endif
